---
type: docs
---

This page contains the Arch Linux legal documentation.

For trademark policy, please see https://wiki.archlinux.org/title/DeveloperWiki:TrademarkPolicy

Git repository: https://gitlab.archlinux.org/archlinux/service-agreements
